package com.stcs.bpm.ordermanagement.route;

import org.springframework.stereotype.Component;

@Component
public class SampleRoute implements ISampleRoute {

	
	/* (non-Javadoc)
	 * @see com.stcs.bpm.ordermanagement.route.ISampleRoute#printText()
	 */
	@Override
	public String printText() {
		return "testing autowired features";
		
	}
}
