package com.stcs.bpm.ordermanagement.route;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StartOrderProcessRoute extends SpringRouteBuilder {

	@Value("rabbitmq://{{broker.url}}/{{events.topic.name}}?autoAck=false&username={{broker.username}}"
			+ "&password={{broker.password}}&vhost={{broker.vhost}}"
			+ "&routingKey=order.added&exchangeType=topic&autoDelete=false&queue=order_added_queue&automaticRecoveryEnabled=true")
	private String inputSource;
	
	@Value("{{routes.to.start-om}}")
	private String routesToStartOm;

	@Override
	public void configure() throws Exception {

		log.debug("StartOrderProcessRoute route created ");

		from(inputSource)
		.unmarshal().json(JsonLibrary.Gson, Map.class)
		.process(new Processor() {
		     public void process(Exchange exchange) throws Exception {
			       //String businessKey =  exchange.getIn().getHeader("CamelFileName").toString()    .split("-")[1] .substring(0, 4);
		    	  exchange.setProperty("CamundaBpmBusinessKey",
		    			  ( (Map) exchange.getIn().getBody() ).get("CamundaBpmBusinessKey"));
			     }
		 })
		.to("log:com.stcs.camel")
	    .to(routesToStartOm);

		
	}

}
