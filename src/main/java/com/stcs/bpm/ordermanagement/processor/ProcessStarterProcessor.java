package com.stcs.bpm.ordermanagement.processor;

import org.springframework.stereotype.Component;


import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ProcessStarterProcessor {

	public void startCloudOrder(String body) {
        log.info("startCloudOrder>>" + body);

	}
}
