package com.stcs.bpm.ordermanagement.repository.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "order_event")
@Data
public class OrderEvent implements Serializable{
	
	
	private static final long serialVersionUID = 1686229569425151262L;
	
	@Id
	private Integer id;
	
	@JoinColumn(name = "order_id")
	@ManyToOne
	private Order order;
	
	@Column(name = "subscription_id")
	private String subscriptionId;
	
	@Column(name = "creation_date")
	private LocalDate creationDate;
	
	@ManyToOne
	@JoinColumn(name = "status", insertable = false, updatable = false)
	private OrderStatus orderStatus;
	

}
