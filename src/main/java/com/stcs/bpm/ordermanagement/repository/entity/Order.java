package com.stcs.bpm.ordermanagement.repository.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "orders")
@Data
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "subscription_id")
	private String subscriptionId;
	
	@Column(name = "acct_id")
	private String accountId;
	
	@Column(name = "plan_name")
	private String planeName;
	
	@Column(name = "start_date")
	private LocalDate startDate;
	
	@Column(name = "served")
	private Boolean served;

}
