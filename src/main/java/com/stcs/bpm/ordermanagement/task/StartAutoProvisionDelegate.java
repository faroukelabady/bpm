package com.stcs.bpm.ordermanagement.task;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.test.context.TestPropertySource;

import com.google.gson.JsonObject;
import com.stcs.bpm.clients.RabbitMQClient;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
@PropertySource("classpath:application.yaml")
public class StartAutoProvisionDelegate implements JavaDelegate {

    @Autowired
    private RabbitMQClient rabbitMQClient ;
    
    
    //TODO : read from properties.yaml
    private static String ROUTING_KEY = "cloud.provision.request" ;
    
    
    public void execute(DelegateExecution execution) throws Exception {
		    
		    String planName          =  (String) execution.getVariable("planName") ;
		    String BpmBusinessKey    =  (String)  execution.getVariable("CamundaBpmBusinessKey") ;
		    String accountKey        =  (String)  execution.getVariable("accountKey") ;
   
	    	//TODO order SAVE IN DB

		    
		    JsonObject msgJson = new JsonObject();
		    msgJson.addProperty("planName", planName) ;
		    msgJson.addProperty("BpmBusinessKey", BpmBusinessKey ) ;
		    msgJson.addProperty("accountKey", accountKey) ;		    
		    
		    rabbitMQClient.send(msgJson.toString(), ROUTING_KEY) ;
		    
	}
}