package com.stcs.bpm.ordermanagement.task;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CheckProvisionResult implements JavaDelegate {
   
    public void execute(DelegateExecution execution) throws Exception {
		     log.info("executed CheckProvisionResult-> provisionStatus:  " + execution.getVariable("provisionStatus")  );
		   
		    if(  execution.getVariable("provisionStatus") != null  ) {
		    	String provisionStatus = (String) execution.getVariable("provisionStatus")  ;
		    	//TODO SAVE IN DB
		    	
		    }else {
		    	// TODO HANDLE TIMEOUT 
		    }
		    
    } 

}
