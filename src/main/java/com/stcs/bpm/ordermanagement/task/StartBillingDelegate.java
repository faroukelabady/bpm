package com.stcs.bpm.ordermanagement.task;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StartBillingDelegate implements JavaDelegate  {
	
    public void execute(DelegateExecution execution) throws Exception {
	     log.info("executed StartBillingDelegate-> serviceType:  " + execution.getVariable("serviceType")  );
	     // TODO : send subscription jms message to RMQ 
         // TODO : save status in DB
	     
	     
    }
}
