package com.stcs.bpm;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.camunda.bpm.camel.component.CamundaBpmComponent;
import org.camunda.bpm.camel.spring.CamelServiceImpl;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableProcessApplication
public class BpmApplication {
	
	@Autowired
	CamelContext camelContext;

	@Autowired
	ProcessEngine processEngine;

	public static void main(String[] args) {
		SpringApplication.run(BpmApplication.class, args);
	}

	@Bean(name = "camel")
	public CamelServiceImpl camel() {
		CamelServiceImpl camelServiceImpl = new CamelServiceImpl();
		camelServiceImpl.setCamelContext(camelContext);
		camelServiceImpl.setProcessEngine(processEngine);
		return camelServiceImpl;
	}

	@Bean
	CamelContextConfiguration nameConfiguration() {
		return new CamelContextConfiguration() {
			@Override
			public void beforeApplicationStart(CamelContext camelContext) {
				CamundaBpmComponent component = new CamundaBpmComponent(processEngine);
				SpringProcessEngineConfiguration config = (SpringProcessEngineConfiguration) processEngine
						.getProcessEngineConfiguration();
				config.getBeans().put("camel", camel());
				camelContext.addComponent("camunda-bpm", component);
			}

			@Override
			public void afterApplicationStart(CamelContext camelContext) {

			}
		};
	}

}
