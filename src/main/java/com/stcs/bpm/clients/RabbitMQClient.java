package com.stcs.bpm.clients;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQClient implements BrokerClient {

	@Autowired
	RabbitConnection rabbitConnection ;

    @Value("${events.topic.name}")
    private String topic;

	public boolean send(String message, String routingKey)
			throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException {
		
		    	rabbitConnection.send(message, routingKey, topic);
				return true;
	
	}

   
}
