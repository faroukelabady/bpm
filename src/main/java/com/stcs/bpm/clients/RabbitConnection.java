package com.stcs.bpm.clients;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Singleton
@Component
public class RabbitConnection {
	
	    @Value("${broker.url}")
	    private String url;
	    
	    @Value("${broker.username}")
	    private String username;

	    @Value("${broker.password}")
	    private String password;
	    
	    @Value("${broker.vhost}")
	    private String vhost;
	    
	    private Connection connection  ; 

	    
	    private Connection getConnection() throws IOException, TimeoutException {
	    	
	    	 if(connection == null || !connection.isOpen() ) {
	    		 ConnectionFactory factory = new ConnectionFactory();
		         factory.setUsername(username);
		         factory.setPassword(password);
		         factory.setVirtualHost(vhost);
		         factory.setHost(url);
		         factory.setRequestedHeartbeat(30);
		         factory.setConnectionTimeout(30000);
		         
		         connection =  factory.newConnection() ;
	    	 }
	    	return connection ;
	    }
	    
	    
	    public boolean send(String message, String routingKey , String topic) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException {
	        
	    	AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
	                .contentType("application/json")
	                .deliveryMode(2)
	                .priority(1)
	                .build();

	        Channel channel = getConnection().createChannel();

	        channel.basicPublish(topic, routingKey, properties, message.getBytes());
	        
	        channel.close();
	        connection.close();
			return true;
	    }
}
