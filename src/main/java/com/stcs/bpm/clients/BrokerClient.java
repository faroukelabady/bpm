package com.stcs.bpm.clients;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

@Component
public interface BrokerClient {
    boolean send(String message,String routingKey) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException;
}
