package com.stcs.bpm;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "orderManagementEntityManagerFactory", transactionManagerRef = "orderManagementTransactionManager",
basePackages = {"com.stcs.bpm.ordermanagement.repository" })
public class OrderManagementDbConfig {

	@Bean
	@Primary
	@ConfigurationProperties(prefix = "order.management.datasource")
	public DataSource orderManagementDataSource() {
		 return DataSourceBuilder.create().build();
	}
	
	@Bean(name="camundaBpmDataSource")
	@ConfigurationProperties(prefix="spring.datasource")
	public DataSource secondaryDataSource() {
	  return DataSourceBuilder.create().build();
	}

	 @Bean(name = "orderManagementEntityManagerFactory")
	 public LocalContainerEntityManagerFactoryBean
	 orderManagementEntityManagerFactory(EntityManagerFactoryBuilder builder,
	 @Qualifier("orderManagementDataSource") DataSource dataSource) {
	 System.out.println("is datasource exist"+ dataSource);
	 return
	 builder.dataSource(dataSource).packages("com.stcs.bpm.ordermanagement.repository.entity").persistenceUnit("orderManagment").build();
	 }
	
	 @Bean(name = "orderManagementTransactionManager")
	 public PlatformTransactionManager orderManagementTransactionManager(
	 @Qualifier("orderManagementEntityManagerFactory") EntityManagerFactory
	 orderManagementEntityManagerFactory) {
	 return new JpaTransactionManager(orderManagementEntityManagerFactory);
	 }

}
