CREATE TABLE orders (
id serial PRIMARY KEY,
subscription_id VARCHAR NOT NULL,
acct_id VARCHAR NOT NULL,
plan_name VARCHAR NOT NULL,
start_date DATE NOT NULL,
served BOOLEAN
);

CREATE TABLE order_status (
code VARCHAR PRIMARY KEY,
name VARCHAR NOT NULL
);


CREATE TABLE order_event (
id bigserial PRIMARY KEY,
order_id INT REFERENCES orders(id),
subscription_id VARCHAR NOT NULL,
status VARCHAR REFERENCES order_status(code),
creation_date DATE NOT NULL,
comment VARCHAR NOT NULL
);


INSERT INTO order_status VALUES ('START', 'Start order process');
INSERT INTO order_status VALUES ('SEND_PROV', 'Send provision order');
INSERT INTO order_status VALUES ('RECEV_PROV_RESULT', 'Receieved provision order result');
INSERT INTO order_status VALUES ('SEND_POS', 'Send POS service start service message');
INSERT INTO order_status VALUES ('SEND_BILLING', 'Send billing start service message');
INSERT INTO order_status VALUES ('SUCCESS', 'Order delivered successfully');
INSERT INTO order_status VALUES ('FAILED', 'Order process failed');
